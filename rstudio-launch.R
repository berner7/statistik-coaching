library(learnr)
initialize_tutorial()
setwd("~/inst/")
rmarkdown::run(file = "coaching.Rmd", shiny_args = list(launch.browser = TRUE))

